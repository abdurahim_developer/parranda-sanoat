$(document).ready(function () {
    $('.header__burger').click(function (event) {
        $('.header__burger, .header__menu, body').toggleClass('active');
        $('body').toggleClass('lock');
    });
});

$(document).ready(function () {
    // Set event listeners
    if (document.querySelector('#print-btn')) {
        document.querySelector('#print-btn').addEventListener('click', () => {
            window.print();
        })
    }
});

const swiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },


    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 1,
            spaceBetween: 30
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        991: {
            slidesPerView: 3,
            spaceBetween: 30
        }
    }
});

const partner = new Swiper('.partner-swiper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },


    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 40
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        991: {
            slidesPerView: 4,
            spaceBetween: 40
        }
    }
});


// document.querySelectorAll('.menu_active').forEach(function (menu) {
// 	menu.addEventListener('mouseover', function(e) {
// 		e.target.nextElementSibling.style.display = 'block';
// 	});
// 	menu.addEventListener('mouseout', function(e) {
// 		e.target.nextElementSibling.style.display = 'none';
// 	});
// });

if($('#tilt').length) {

    /* Store the element in el */
    let el = document.getElementById('tilt');

    /* Get the height and width of the element */
    const height = el.clientHeight;
    const width = el.clientWidth;

    /*
      * Add a listener for mousemove event
      * Which will trigger function 'handleMove'
      * On mousemove
      */
    el.addEventListener('mousemove', handleMove);

    /* Define function a */
    function handleMove(e) {
    /*
      * Get position of mouse cursor
      * With respect to the element
      * On mouseover
      */
    /* Store the x position */
    const xVal = e.layerX;
    /* Store the y position */
    const yVal = e.layerY;

    /*
      * Calculate rotation valuee along the Y-axis
      * Here the multiplier 20 is to
      * Control the rotation
      * You can change the value and see the results
      */
    const yRotation = 20 * ((xVal - width / 2) / width);

    /* Calculate the rotation along the X-axis */
    const xRotation = -20 * ((yVal - height / 2) / height);

    /* Generate string for CSS transform property */
    const string = 'perspective(500px) scale(1.1) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'

    /* Apply the calculated transformation */
        el.style.transform = string;
    }

    /* Add listener for mouseout event, remove the rotation */
    el.addEventListener('mouseout', function () {
        el.style.transform = 'perspective(500px) scale(1) rotateX(0) rotateY(0)'
    });

    /* Add listener for mousedown event, to simulate click */
    el.addEventListener('mousedown', function () {
        el.style.transform = 'perspective(500px) scale(0.9) rotateX(0) rotateY(0)'
    });

    /* Add listener for mouseup, simulate release of mouse click */
    el.addEventListener('mouseup', function () {
        el.style.transform = 'perspective(500px) scale(1.1) rotateX(0) rotateY(0)'
    });

}

// AOS.init()


// let menu_item = document.querySelector('.menu_active');
// let drop_item = document.querySelector('.menu-item__dropdown');


// let menu_item = document.querySelector('.menu_active');
// let drop_item = document.querySelector('.menu-item__dropdown');
// menu_item.addEventListener('mouseover', () => {
// 	drop_item.css.display = 'block';
// });
// menu_item.addEventListener('mouseout', () => {
// 	drop_item.css.display = 'none';
// });

$(document).ready(function () {
    $(".header__menu").find(".menu-media__dropdown").siblings('a').on("click", function () {
        $(this).siblings(".menu-media__dropdown").toggleClass("menu-media__dropdown_active");
    });
    $(".header__menu").find(".menu-media__dropdown").siblings('a').on("click", function () {
        $(".fa .fa-chevron-right").toggleClass("icon_rotate");
    });
    $(".header_bottom_hamburger").on("click", function () {
        $('.header__burger, .header__menu, body').removeClass('active');
    });

    $(".mega-menu__hamburger").on("click", function () {
        $(".mega__menu").addClass("mega__menu_active");
    });
    $(".mega-menu__closer").on("click", function () {
        $(".mega__menu").removeClass("mega__menu_active");
    });
    $(".header__menu .menu-media__dropdown").find(".menu-meida__dropdown-inside").siblings("a").on("click", function () {
        $(this).siblings(".menu-meida__dropdown-inside").toggleClass("menu-meida__dropdown-inside_active")
    });



});
