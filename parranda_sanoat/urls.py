import patterns as patterns
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.i18n import i18n_patterns

from . import settings
from main import views as main_views


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
)

urlpatterns += [
    path('', include('main.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        re_path(r'^rosetta/', include('rosetta.urls'))
    ]

handler500 = 'main.views.error_500'
handler404 = 'main.views.error_404'

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
