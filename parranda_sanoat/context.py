from main import models


def defaults(req):
    return {
        "menus": models.Menu.objects.filter(parent__isnull=True),
        "site_settings": models.SiteSettings.objects.first(),
        'categories': models.CategoryProduct.objects.filter(parent__isnull=True),
        "about": models.About.objects.first(),
    }
