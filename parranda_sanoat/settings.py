
import os
from django.conf import global_settings
from django.utils.translation import gettext_lazy as _
import django.conf.locale
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&n17htv#jlx#$x2hqi-_n5+f2a2x!+lg%f9w5zn8n=s*%5bdii'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'rosetta',
    'parler',
    'adminlte3',
    # Optional: Django admin theme (must be before django.contrib.admin)
    'adminlte3_theme',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',


    # third party
    'mptt',
    'ckeditor',
    'ckeditor_uploader',
    'adminsortable2',
    'simple_open_graph',

    # APP
    'main',
]

SITE_ID = 1

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'main.middleware.ForceLangMiddleware', # default language
    'django.middleware.locale.LocaleMiddleware', # default language
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'parranda_sanoat.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                    # for context
                    'parranda_sanoat.context.defaults'
            ],
        },
    },
]

WSGI_APPLICATION = 'parranda_sanoat.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'uz'

TIME_ZONE = 'Asia/Tashkent'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

# CHANGE SETTINGS

# For static
STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
STATIC_ROOT = os.path.join(BASE_DIR, 'assets')

# For Media
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Django CKEDITOR
CKEDITOR_UPLOAD_PATH = 'utils.get_filename'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': None,
    },
}

# Django Parler

LANGUAGES = [
    ('uz', "Uz"),
    ('ru', "Ru"),
    ('en', "Eng"),
]

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale')
]

PARLER_DEFAULT_LANGUAGE_CODE = 'uz'

PARLER_LANGUAGES = {
    SITE_ID: (
        {'code': 'uz', },
        {'code': 'ru', },
        {'code': 'en', },

    ),
    'default': {
        'fallbacks': [PARLER_DEFAULT_LANGUAGE_CODE],  # defaults to PARLER_DEFAULT_LANGUAGE_CODE
        'hide_untranslated': False,  # the default; let .active_translations() return fallbacks too.
    }
}

PARLER_ENABLE_CACHING = False

# Django Mptt
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'testemailsend3@gmail.com'
EMAIL_HOST_PASSWORD = 'Testemail24'
# DEFAULT_FROM_EMAIL = 'abduraximdeveloper@gmail.com'
DEFAULT_TO_EMAIL = 'info@parrandasanoat.uz'

#rosetta

ROSETTA_AUTO_COMPILE: True
ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'
