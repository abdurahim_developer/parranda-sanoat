from django import forms
from django.utils.translation import gettext as _


class ContactForm(forms.Form):
    """ CONTACT FORM """
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': _('Your name')}
        ),
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'placeholder': _('Your e-mail')}
        )
    )
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': _('Your phone number')}
        )
    )
    theme = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': _('Theme of appeal')}
        )
    )
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder': _('Your message')}
        )
    )
