# Generated by Django 3.1.5 on 2021-03-02 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_sitesettings_home_page_quate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sitesettings',
            name='home_page_quate',
        ),
        migrations.AddField(
            model_name='sitesettingstranslation',
            name='home_page_quote',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='sitesettingstranslation',
            name='home_page_quote_small_text',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
