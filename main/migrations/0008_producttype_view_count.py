# Generated by Django 3.1.5 on 2021-02-26 09:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20210225_1359'),
    ]

    operations = [
        migrations.AddField(
            model_name='producttype',
            name='view_count',
            field=models.IntegerField(default=0, null=True, verbose_name='View Count'),
        ),
    ]
