from django import template
from django.utils.translation import get_language

register = template.Library()


@register.simple_tag
def get_url_tag(request, lang):
    active_language = get_language()
    return request.get_full_path().replace(active_language, lang, 1)


@register.filter
def split(text, delimiter):
    return str(text).split(delimiter)


@register.filter
def get_prop(obj, prop):
    try:
        return getattr(obj, prop)
    except AttributeError:
        return obj[prop]
    except AttributeError:
        return None
