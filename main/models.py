from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from parler import models as parler_models
from mptt import models as mptt_models
from ckeditor_uploader import fields as ckeditor_uploader_fields
from autoslug import AutoSlugField

from . import managers


class Menu(mptt_models.MPTTModel, parler_models.TranslatableModel):
    """ MENU MODEL """
    parent = models.ForeignKey('self', related_name='children', on_delete=models.CASCADE, blank=True, null=True,
                               verbose_name=_('Parent'))
    translations = parler_models.TranslatedFields(
        name=models.CharField(max_length=128, blank=False, default='', verbose_name=_('Name')),
        path=models.CharField(max_length=128, blank=True, null=True, default='', verbose_name=_('Path')),
    )
    objects = managers.CustomMpttManager()

    def __str__(self):
        return self.safe_translation_getter('name', any_language=True) or ''

    class Meta:
        verbose_name = _('Menu Item')
        verbose_name_plural = _('Menu')


class Post(parler_models.TranslatableModel):
    """ POST MODEL """
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, verbose_name=_('Title')),
        description=models.TextField(null=True, blank=True),
        content=ckeditor_uploader_fields.RichTextUploadingField(verbose_name=_('Content')),
    )
    main_image = models.ImageField(upload_to='uploads/news/%Y/%m/%d', verbose_name=_('Main Image'))
    view_count = models.IntegerField(default=0, null=True, verbose_name=_('View Count'))
    is_active = models.BooleanField(default=False, verbose_name=_('Is Active'))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_absolute_url(self, lang=None):
        if lang:
            return reverse('main:post_detail_url', args=[lang, self.id])
        else:
            return reverse('main:post_detail_url', args=[self.id])

    def __str__(self):
        return f"{self.safe_translation_getter('title')}"

    class Meta:
        verbose_name = _('News Item')
        verbose_name_plural = _('News')

class CategoryProduct(mptt_models.MPTTModel, parler_models.TranslatableModel):
    """CATEGORY MODEL"""
    parent = models.ForeignKey('self', default='', related_name='children', on_delete=models.CASCADE, blank=True,
                               null=True, verbose_name=_('Parent'))
    translations = parler_models.TranslatedFields(
        name=models.CharField(max_length=255, null=True, verbose_name=_('Name')),
        slug=AutoSlugField(populate_from='name', null=True, allow_unicode=True, always_update=True,
                           verbose_name=_('Slug')),
    )
    objects = managers.CustomMpttManager()

    def __str__(self):
        return f"{self.safe_translation_getter('name')}"

    class Meta:
        verbose_name = _('Category Item')
        verbose_name_plural = _('Category Products')

class Product(parler_models.TranslatableModel):
    """ PRODUCT MODEL """
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, verbose_name=_('title')),
        description=models.TextField(verbose_name=_('Description'), null=True),
        content=ckeditor_uploader_fields.RichTextUploadingField(verbose_name=_('Content')),
    )

    category = models.ForeignKey(CategoryProduct, on_delete=models.CASCADE, null=True, verbose_name=_('Category'))
    main_image = models.ImageField(upload_to='uploads/product/%Y/m/%d', verbose_name=_('Main Image'))
    view_count = models.IntegerField(default=0, null=True, verbose_name=_('View Count'))

    order = models.PositiveIntegerField(default=0, blank=False, null=False, verbose_name=_('Order'))
    is_active = models.BooleanField(default=False, verbose_name=_('Is Active'))

    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))
    updated = models.DateTimeField(auto_now=True, verbose_name=_('Updated'))

    def get_absolute_url(self, lang=None):
        if lang:
            return reverse('main:product_detail_url', args=[lang, self.id])
        else:
            return reverse('main:product_detail_url', args=[self.id])

    def __str__(self):
        return f"{self.safe_translation_getter('title')}"

    class Meta:
        ordering = ['order']
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class About(parler_models.TranslatableModel):
    """About MODEL"""
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, null=True, verbose_name=_('Title')),
        description=ckeditor_uploader_fields.RichTextUploadingField(verbose_name=_('description')),
    )
    main_image = models.ImageField(upload_to='uploads/about/%Y/%m/%d', verbose_name=_('Main Image'))

    def str(self):
        return f"{self.safe_translation_getter('title')}"

    class Meta:
        verbose_name = _('About')
        verbose_name_plural = _('About Us')


class UsefulSite(parler_models.TranslatableModel):
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, verbose_name=_('Title')),
        link=models.CharField(max_length=255, blank=True, default='', verbose_name=_('Link'))
    )
    image = models.ImageField(upload_to='uploads/useful/', verbose_name=_('Image'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))

    def __str__(self):
        return f"{self.safe_translation_getter('title')}"

    class Meta:
        verbose_name = _('Useful Site')
        verbose_name_plural = _('Useful Sites')


class PhotoGallery(parler_models.TranslatableModel):
    """ PHOTO GALLERY MODEL """
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, blank=True, verbose_name=_('Title')),
        content=ckeditor_uploader_fields.RichTextUploadingField(blank=True, null=True, verbose_name=_('Content')),
    )
    image = models.ImageField(upload_to='uploads/gallery/%Y/%m/%d', verbose_name=_('Image'))
    is_active = models.BooleanField(default=False, verbose_name=_('Is active'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))

    def __str__(self):
        return f"{self.safe_translation_getter('title')}"

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')


class GalleryImage(models.Model):
    gallery = models.ForeignKey(PhotoGallery, on_delete=models.CASCADE, related_name='sub_galleries',
                                verbose_name=_('Gallery'))
    image = models.ImageField(upload_to='gallery/images/', verbose_name=_('Image'))
    title = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Title'))


class Video(models.Model):
    """ VIDEO MODEL """
    title = models.CharField(max_length=255, blank=True, verbose_name=_('Title'))
    video = models.CharField(max_length=500, blank=True, null=True, verbose_name=_('Video'))
    description = models.TextField(blank=True, null=True, verbose_name=_('Description'))
    is_active = models.BooleanField(default=False, verbose_name=_('Is active'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Video')
        verbose_name_plural = _('Videos')


class Partner(models.Model):
    """ PARTNER MODEL """
    title = models.CharField(max_length=255, blank=True, verbose_name=_('Title'))
    path = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Path'))
    image = models.ImageField(upload_to='uploads/partner/%Y/%m/%d', verbose_name=_('Image'))
    is_active = models.BooleanField(default=False, verbose_name=_('Is Active'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Partner')
        verbose_name_plural = _('Partners')


class SiteSettings(parler_models.TranslatableModel):
    """ SITE SETTINGS MODEL """
    translations = parler_models.TranslatedFields(
        address=models.CharField(max_length=255, blank=True, verbose_name=_('Address')),
        operating_mode=models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Operating Mode')),
        content=models.TextField(verbose_name=_('Content')),
        home_page_quote_1=models.CharField(max_length=255, null=True, blank=True),
        home_page_quote_small_text_1=models.CharField(max_length=255, null=True, blank=True),
        home_page_quote_2=models.CharField(max_length=255, null=True, blank=True),
        home_page_quote_small_text_2=models.CharField(max_length=255, null=True, blank=True),
    )
    logo = models.ImageField(upload_to='uploads/logo/', verbose_name=_('Image'))
    tel_first = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Tel First'))
    tel_second = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Tel Second'))
    tel_third = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Tel Third'))
    fax = models.CharField(max_length=255, blank=True, null=True)
    email_1 = models.EmailField(blank=True, null=True)
    email_2 = models.EmailField(blank=True, null=True)
    email_3 = models.EmailField(blank=True, null=True)

    facebook = models.URLField(max_length=500, blank=True, null=True)
    instagram = models.URLField(max_length=500, blank=True, null=True)
    telegram = models.URLField(max_length=500, blank=True, null=True)
    twitter = models.URLField(max_length=500, blank=True, null=True)
    youtube = models.URLField(max_length=500, blank=True, null=True)

    home_page_quote_1_bg = models.ImageField(upload_to="uploads/site_settings/%Y/%m/%d", null=True)
    home_page_quote_2_bg = models.ImageField(upload_to="uploads/site_settings/%Y/%m/%d", null=True)

    home_page_header_bg = models.ImageField(upload_to="uploads/site_settings/%Y/%m/%d", null=True)

    class Meta:
        verbose_name = _('Site Settings')
        verbose_name_plural = _('Site Settings')


class StaticContent(parler_models.TranslatableModel):
    """ Static Content MODEL """
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, verbose_name=_('Title')),
        content=ckeditor_uploader_fields.RichTextUploadingField(verbose_name=_('Content')),
    )
    image = models.ImageField(upload_to='uploads/static', verbose_name=_('Image'))
    view_count = models.IntegerField(default=0, null=True, verbose_name=_('View Count'))

    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    def __str__(self): return f"{self.safe_translation_getter('title', '-')}"

    class Meta:
        verbose_name = _('Static Content')
        verbose_name_plural = _('Static Content')


class Leader(parler_models.TranslatableModel):
    translations = parler_models.TranslatedFields(
        name=models.CharField(max_length=255, verbose_name=_('Name')),
        position=models.CharField(max_length=255, verbose_name=_('Position')),
    )
    image = models.ImageField(upload_to='uploads/leader/', verbose_name=_('Image'))
    internal_telephone = models.CharField(max_length=255, verbose_name=_('Internal Telephone'))
    city_service_phone = models.CharField(max_length=255, verbose_name=_('City Service Phone'))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self): return f"{self.safe_translation_getter('name', '-')}"

    class Meta:
        verbose_name = _('Leader')
        verbose_name_plural = _('Leaders')


class Enterprise(parler_models.TranslatableModel):
    translations = parler_models.TranslatedFields(
        name=models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Name')),
        title=models.CharField(max_length=255, verbose_name=_('Position')),
        address=models.CharField(max_length=500, blank=True, verbose_name=_('Address'))
    )
    image = models.ImageField(upload_to='uploads/enterprise/', verbose_name=_('Image'))
    tel = models.CharField(max_length=50, blank=True, verbose_name=_('Tel'))
    fax = models.CharField(max_length=50, blank=True, verbose_name=_('Fax'))

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.safe_translation_getter('title', '-')}"

    class Meta:
        verbose_name = _('Enterprise')
        verbose_name_plural = _('Enterprise')


class Chairmen(parler_models.TranslatableModel):
    translations = parler_models.TranslatedFields(
        name=models.CharField(max_length=255, verbose_name=_('Name')),
        position=models.CharField(max_length=255, verbose_name=_('Position')),
        working_period=models.CharField(max_length=250, blank=True, verbose_name=_('Working Period')),
    )
    image = models.ImageField(upload_to='uploads/chairmen/', verbose_name=_('Image'))
    working_time = models.CharField(max_length=100, blank=True, verbose_name=_('Working Time'))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self): return f"{self.safe_translation_getter('name', '-')}"

    class Meta:
        verbose_name = _('Chairmen')
        verbose_name_plural = _('Chairmen')


class Veterans(parler_models.TranslatableModel):
    translations = parler_models.TranslatedFields(
        name=models.CharField(max_length=255, verbose_name=_('Name')),
        position=models.CharField(max_length=255, verbose_name=_('Position')),
        working_period=models.CharField(max_length=250, blank=True, verbose_name=_('Working Period')),
    )
    image = models.ImageField(upload_to='uploads/chairmen/', verbose_name=_('Image'))
    address = models.CharField(max_length=100, blank=True, verbose_name=_('Address'))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self): return f"{self.safe_translation_getter('name', '-')}"

    class Meta:
        verbose_name = _('Veterans')
        verbose_name_plural = _('Veterans')


class ProductType(parler_models.TranslatableModel):
    translations = parler_models.TranslatedFields(
        title=models.CharField(max_length=255, verbose_name=_('Title')),
        content=ckeditor_uploader_fields.RichTextUploadingField(verbose_name=_('Content')),
        image=models.ImageField(upload_to='uploads/product_type/'),
    )
    view_count = models.IntegerField(default=0, null=True, verbose_name=_('View Count'))
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_absolute_url(self, lang=None):
        if lang:
            return reverse('main:poultry_detail_url', args=[self.id])
        else:
            return reverse('main:poultry_detail_url', args=[lang, self.id])

    def __str__(self): return f"{self.safe_translation_getter('title', '-')}"

    class Meta:
        verbose_name = _('Poultry Type')
        verbose_name_plural = _('Poultry Type')
