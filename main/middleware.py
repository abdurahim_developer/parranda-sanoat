from django.conf import settings
from django.utils import translation
from django.utils.deprecation import MiddlewareMixin


class ForceLangMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if not translation.get_language_from_path(request.path):
            request.META['HTTP_ACCEPT_LANGUAGE'] = settings.PARLER_DEFAULT_LANGUAGE_CODE
        return None
