from django.contrib import admin
from django.contrib.auth.models import Group, User
from django.forms import ClearableFileInput
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from adminsortable2 import admin as adminsortable2_admin
from mptt import admin as mptt_admin
from parler import admin as parler_admin

from . import models

admin.site.site_header = _("Poultry Industry")
admin.site.index_title = _("Administration")
admin.site.site_title = _("Administration")

admin.site.unregister(Group)
admin.site.unregister(User)


@admin.register(models.Menu)
class MenuAdmin(parler_admin.TranslatableAdmin, mptt_admin.DraggableMPTTAdmin):
    list_display = (
        'tree_actions',
        'indented_title'
    )
    list_display_links = (
        'indented_title',
    )
    mptt_level_indent = 20

    # def get_available_languages(self, obj):
    #     return [LANGUAGE[0] for LANGUAGE in settings.LANGUAGES]


@admin.register(models.Post)
class PostAdmin(parler_admin.TranslatableAdmin):
    list_display = ['title', 'is_active', 'view_count', 'created', 'updated', 'get_image']
    list_display_links = ['title']
    list_editable = ['is_active']
    readonly_fields = ['view_count']

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.main_image.url} width="auto" height="60"')

    get_image.short_description = _("Image")


@admin.register(models.Product)
class ProductAdmin(adminsortable2_admin.SortableAdminMixin, parler_admin.TranslatableAdmin):
    ordering = ['order']
    list_display = ['id', 'title', 'is_active', 'created', 'updated', 'get_image']
    list_display_links = ['title']
    list_editable = ['is_active']
    readonly_fields = ['view_count']

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.main_image.url} width="auto" height="60"')

    get_image.short_description = _("Image")

@admin.register(models.CategoryProduct)
class CategoryProductAdmin(parler_admin.TranslatableAdmin, mptt_admin.DraggableMPTTAdmin):
    list_display = (
        'id',
        'tree_actions',
        'indented_title'
    )
    list_display_links = (
        'indented_title',
    )
    mptt_level_indent = 20


@admin.register(models.About)
class AboutAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'title']
    list_display_links = ['title']

    def has_add_permission(self, request):
        return False


class GalleryImageInline(admin.TabularInline):
    model = models.GalleryImage


@admin.register(models.PhotoGallery)
class PhotoGalleryAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'title', 'is_active', 'created', 'get_image']
    list_display_links = ['title']
    list_editable = ['is_active']
    inlines = [GalleryImageInline]

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="auto" height="60"')

    get_image.short_description = _("Image")


@admin.register(models.Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'is_active', 'created']
    list_editable = ['is_active']
    list_display_links = ['title']


@admin.register(models.Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'is_active', 'created', 'get_image']
    list_editable = ['is_active']
    list_display_links = ['id','title']

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="auto" height="60"')

    get_image.short_description = _("Image")


@admin.register(models.UsefulSite)
class UseFulSiteAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'title', 'created', 'get_image']

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="auto" height="60"')

    get_image.short_description = _("Image")


@admin.register(models.SiteSettings)
class SiteSettingsAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'address']
    list_display_links = ['id']
    fields = [
        'address',
        'operating_mode',
        'content',
        'home_page_header_bg',
        'home_page_quote_1',
        'home_page_quote_small_text_1',
        'home_page_quote_1_bg',
        'home_page_quote_2',
        'home_page_quote_small_text_2',
        'home_page_quote_2_bg',
        'logo',
        'tel_first',
        'tel_second',
        'tel_third',
        'fax',
        'email_1',
        'email_2',
        'email_3',
        'facebook',
        'instagram',
        'telegram',
        'twitter',
        'youtube',
    ]

    def has_add_permission(self, request):
        return False


@admin.register(models.StaticContent)
class StaticContentAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'title', 'view_count', 'created']
    list_display_links = ['id', 'title']
    readonly_fields = ['view_count']


@admin.register(models.Leader)
class LeaderAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'name', 'position', 'internal_telephone', 'city_service_phone']
    list_display_links = ['id', 'name']


@admin.register(models.Enterprise)
class EnterpriseAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'title', 'tel', 'fax', 'created']
    list_display_links = ['id', 'title']


@admin.register(models.Chairmen)
class ChairmenAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'name', 'position', 'created']
    list_display_links = ['id', 'name']


@admin.register(models.Veterans)
class VeteransAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'name', 'position', 'working_period', 'created']
    list_display_links = ['id', 'name']


@admin.register(models.ProductType)
class PoultryTypeAdmin(parler_admin.TranslatableAdmin):
    list_display = ['id', 'title', 'created']
    list_display_links = ['id', 'title']
    readonly_fields = ['view_count']
