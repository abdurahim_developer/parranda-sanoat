from django.urls import path
from . import views
from django.conf.urls import url, include, handler404, handler500

app_name = 'main'


urlpatterns = [
    path('<lang>/', views.HomeView.as_view(), name='home_url'),
    path('<lang>/about', views.AboutView.as_view(), name='about _url'),
    path('<lang>/contacts/', views.ContactsView.as_view(), name='home_contact'),
    path('<lang>/news/', views.PostView.as_view(), name='news_url'),
    path('<lang>/news/<int:id>', views.PostDetailView.as_view(), name='post_detail_url'),
    path('<lang>/product/', views.ProductView.as_view(), name='product_url'),
    # path('<lang>/search/', views.ProductSearchView.as_view(), name='product_search_url'),
    path('<lang>/search/', views.SearchView.as_view(), name='product_search_url'),
    path('<lang>/product/<slug:category_slug>/', views.ProductByCategory.as_view(), name='product_by_category_url'),
    path('<lang>/product/<int:id>', views.ProductDetailView.as_view(), name='product_detail_url'),
    path('<lang>/gallery/', views.GalleryView.as_view(), name='gallery_url'),
    path('<lang>/gallery/<int:id>', views.GalleryDetail.as_view(), name='gallery_detail_url'),
    path('<lang>/video', views.VideoView.as_view(), name='video_url'),
    path('<lang>/static/<int:id>', views.StaticDetail.as_view(), name="static-detail"),
    path('<lang>/leader/', views.LeaderList.as_view(), name="leader-url"),
    path('<lang>/enterprise/', views.EnterpriseList.as_view(), name="enterprise_url"),
    path('<lang>/veterans/', views.VeteransList.as_view(), name="veterans_url"),
    path('<lang>/chairmen/', views.ChairmenList.as_view(), name="chairmen_url"),
    path('<lang>/poultry/', views.PoultryTypeView.as_view(), name="poultry_url"),
    path('<lang>/poultry/<int:id>', views.PoultryDetailView.as_view(), name='poultry_detail_url'),
]

