from django.core.mail import send_mail
from django.forms import model_to_dict
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from parranda_sanoat.settings import DEFAULT_TO_EMAIL, EMAIL_HOST_USER
# from django.shortcuts import render_to_response
from django.template import RequestContext

from . import models
from .forms import ContactForm


def error_404(request, exception):
    return render(request, 'errors/404.html', status=404)


def error_500(request):
    return render(request, 'errors/500.html', status=500)


class IncrementInstance(object):
    def get_object(self):
        obj = super().get_object()
        obj.view_count += 1
        obj.save()
        return obj


class HomeView(generic.TemplateView, generic.FormView):
    template_name = 'pages/home/home.html'
    form_class = ContactForm

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        context['news'] = models.Post.objects.filter(is_active=True).order_by('-created')[:6]
        context['products'] = models.Product.objects.filter(is_active=True).order_by('-created')[:5]
        context['useful_site'] = models.UsefulSite.objects.all()
        context['galleries'] = models.PhotoGallery.objects.filter(is_active=True).order_by('-created')[:6]
        context['partners'] = models.Partner.objects.filter(is_active=True).order_by('-created')
        context['videos'] = models.Video.objects.filter(is_active=True).order_by('-created')[:5]
        return context


class AboutView(generic.ListView):
    model = models.About
    template_name = 'pages/about/about.html'
    context_object_name = 'abouts'


class PostView(generic.ListView):
    model = models.Post
    template_name = 'pages/news/all_news.html'
    context_object_name = 'news'
    paginate_by = 9


class PostDetailView(IncrementInstance, generic.DetailView):
    model = models.Post
    template_name = 'pages/news/new_detail.html'
    pk_url_kwarg = 'id'


class ProductView(generic.ListView):
    queryset = models.Product.objects.all()
    template_name = 'pages/product/all_product.html'
    context_object_name = 'products'

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context['product_categories'] = models.CategoryProduct.objects.all()
        return context

    def get_queryset(self):
        lookup = {}
        if self.request.GET.get("category", False):
            lookup['category_id'] = self.request.GET.get('category')
        return self.queryset.filter(**lookup)


class ProductDetailView(IncrementInstance, generic.DetailView):
    model = models.Product
    template_name = 'pages/product/product_detail.html'
    pk_url_kwarg = 'id'


class ProductSearchView(generic.ListView):
    queryset = models.Product.objects.filter(is_active=True)
    template_name = 'pages/product/search_product_result.html'
    context_object_name = 'products'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductSearchView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        q = self.request.GET.get('search')

        return self.queryset.filter(is_active=True).filter(
            Q(translations__title__icontains=q) |
            Q(translations__content__icontains=q)
        )


class ProductByCategory(generic.ListView):
    queryset = models.Product.objects.filter(is_active=True)
    template_name = 'pages/product/product_by_category.html'
    context_object_name = 'products'

    def get_queryset(self):
        return self.queryset.filter(category__translations__slug=self.kwargs.get('category_slug'))


class GalleryView(generic.ListView):
    queryset = models.PhotoGallery.objects.filter(is_active=True)
    template_name = "pages/gallery/all_gallery.html"
    context_object_name = "galleries"
    paginate_by = 9


class GalleryDetail(generic.DetailView):
    model = models.PhotoGallery
    template_name = 'pages/gallery/gallery_detail.html'
    pk_url_kwarg = 'id'


class VideoView(generic.ListView):
    model = models.Video
    template_name = "pages/video/all_video.html"
    context_object_name = "videos"
    paginate_by = 4


class ContactsView(generic.FormView):
    template_name = 'pages/contact/contact.html'
    form_class = ContactForm

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        theme = request.POST.get('theme')
        message = request.POST.get('message')

        message = f"From: {name} email: {email} phone: {phone} \n theme: {theme} message: {message}"

        email_sent = send_mail('Sarlavha', message, EMAIL_HOST_USER, [DEFAULT_TO_EMAIL])

        if email_sent:
            messages.success(self.request, "Your feedback is accepted!")
        else:
            messages.error(self.request, "At this time we cannot receive your request")

        return redirect(request.path)


class StaticDetail(IncrementInstance, generic.DetailView):
    queryset = models.StaticContent.objects.all()
    template_name = "pages/static/detail.html"
    context_object_name = 'static'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super(StaticDetail, self).get_context_data(**kwargs)
        context['news'] = models.Post.objects.filter(is_active=True).order_by('-created')[:3]
        return context


class LeaderList(generic.ListView):
    queryset = models.Leader.objects.all()
    template_name = 'pages/leader/leader.html'
    context_object_name = 'leaders'


class EnterpriseList(generic.ListView):
    queryset = models.Enterprise.objects.all()
    template_name = 'pages/enterprise/enterprise.html'
    context_object_name = 'enterprise'


class ChairmenList(generic.ListView):
    queryset = models.Chairmen.objects.all()
    template_name = 'pages/chairmen/chairmen.html'
    context_object_name = 'chairmens'


class VeteransList(generic.ListView):
    queryset = models.Veterans.objects.all()
    template_name = 'pages/veterans/list.html'
    context_object_name = 'veterans'


class PoultryTypeView(generic.ListView):
    queryset = models.ProductType.objects.all()
    template_name = 'pages/poultry_type/list.html'
    context_object_name = 'types'


class PoultryDetailView(IncrementInstance, generic.DetailView):
    model = models.ProductType
    template_name = 'pages/poultry_type/detail.html'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super(PoultryDetailView, self).get_context_data(**kwargs)
        context['news'] = models.Post.objects.filter(is_active=True).order_by('-created')[:3]
        return context


class SearchView(generic.ListView):
    template_name = 'pages/search_results.html'
    models = [
        models.Post,
        models.Product,
        models.ProductType,
    ]

    fields = [
        'title',
        'description',
        'content',
    ]

    def get_queryset(self):
        from itertools import chain
        qs = []

        for model in self.models:
            qs += list(chain(model.objects.filter(translations__language_code=self.request.LANGUAGE_CODE).distinct()))

        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SearchView, self).get_context_data(object_list=object_list, **kwargs)

        res = []
        s = self.request.GET.get('search')

        for obj in self.get_queryset():

            existing_fields = []

            existing_fields.append('body') if hasattr(obj, 'body') else ''
            existing_fields.append('title') if hasattr(obj, 'title') else ''
            existing_fields.append('short_content') if hasattr(obj, 'short_content') else ''
            existing_fields.append('full_content') if hasattr(obj, 'full_content') else ''
            existing_fields.append('short_description') if hasattr(obj, 'short_description') else ''
            existing_fields.append('full_description') if hasattr(obj, 'full_description') else ''
            existing_fields.append('desc') if hasattr(obj, 'desc') else ''
            existing_fields.append('description') if hasattr(obj, 'description') else ''
            existing_fields.append('name') if hasattr(obj, 'name') else ''
            existing_fields.append('full_name') if hasattr(obj, 'full_name') else ''
            existing_fields.append('bio') if hasattr(obj, 'bio') else ''
            existing_fields.append('short_desc') if hasattr(obj, 'short_desc') else ''
            existing_fields.append('full_desc') if hasattr(obj, 'full_desc') else ''

            for field in existing_fields:

                o = model_to_dict(obj)
                if hasattr(o, field):
                    if str(s).lower() in str(o[field]).lower():
                        res.append(obj)
                else:

                    try:
                        s_in = obj.safe_translation_getter(field) if obj.safe_translation_getter(field) else ''
                    except:
                        s_in = o[field]

                    if str(s).lower() in str(s_in).lower():
                        if obj not in res:
                            res.append(obj)

        ret = []

        # Add link
        for o in res:
            existing_fields = []

            existing_fields.append('title') if hasattr(o, 'title') else ''
            existing_fields.append('name') if hasattr(o, 'name') else ''
            existing_fields.append('full_name') if hasattr(o, 'full_name') else ''

            for field in existing_fields:
                c = o._meta.verbose_name

                if ":" in str(c) and len(str(c).split(':')) >= 1:
                    cat = str(c).split(':')[1:][0]
                else:
                    cat = str(c)

                if hasattr(o, field):
                    tit = getattr(o, field)
                else:
                    tit = o.safe_translation_getter(field)

                ret.append({
                    "title": tit,
                    "link": o.get_absolute_url(lang=self.request.LANGUAGE_CODE),
                    "type": cat
                })

        retr = {}
        for r in ret:

            if r['type'] not in retr.keys():
                retr[r['type']] = [r]
            else:
                retr[r['type']].append(r)

        # context['items'] = ret
        context['types'] = retr

        return context
